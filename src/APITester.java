
import javax.swing.SwingUtilities;
import at.petritzdesigns.apitester.view.MainFrame;

/**
 * API Tester Entry Point
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class APITester {

    /**
     * Main Execution Point<br>
     * Shows Main Frame in the gui thread
     *
     * @param args arguments (no arguments will be handled)
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame mf = new MainFrame();
                mf.setVisible(true);
            }
        });
    }

}
