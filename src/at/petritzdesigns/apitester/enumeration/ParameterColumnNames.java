package at.petritzdesigns.apitester.enumeration;

import at.petritzdesigns.apitester.model.ParameterTableModel;

/**
 * Enum for Column Names used in ParameterTableModel
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 * @see ParameterTableModel
 */
public enum ParameterColumnNames {
    KEY("Key"),
    VALUE("Value");

    /**
     * User friendly name of the column
     */
    private final String name;

    /**
     * Default Constructor
     *
     * @param name the Name
     */
    private ParameterColumnNames(String name) {
        this.name = name;
    }

    /**
     * Returns Name of Enum
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * ToString Method returns name
     *
     * @return name
     */
    @Override
    public String toString() {
        return getName();
    }
}
