package at.petritzdesigns.apitester.enumeration;

/**
 * Enum for Config Items
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public enum ConfigItem {
    CONFIG_PATH("config_path", "Configuration File Path", DisplayType.SAVE_FILE),
    DATA_PATH("data_path", "Data File Path", DisplayType.SAVE_FILE);

    /**
     * Name of the Config Item (key)
     */
    private final String name;

    /**
     * Description of the Config Item
     */
    private final String description;

    /**
     * DisplayType of the Config Item, needed in the ConfigurationDialog
     *
     * @see DisplayType
     */
    private final DisplayType display;

    /**
     * Default Constructor
     *
     * @param name Name of the Config Item
     * @param description Description of the Config Item
     * @param display DisplayType of the Config Item
     */
    private ConfigItem(String name, String description, DisplayType display) {
        this.name = name;
        this.description = description;
        this.display = display;
    }

    /**
     * Returns the Name of the Config Item
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the Description of the Config Item
     *
     * @return description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the Display Type of the Config Item
     *
     * @return display type
     */
    public DisplayType getDisplayType() {
        return display;
    }

    /**
     * Checks if there is a ConfigItem with this name
     *
     * @param name the Name to check
     * @return true if name is in the list, else null
     */
    public static ConfigItem contains(String name) {
        for (ConfigItem item : values()) {
            if (item.getName().equals(name)) {
                return item;
            }
        }
        return null;
    }

    /**
     * ToString Method returns name of the ConfigItem
     *
     * @return name
     */
    @Override
    public String toString() {
        return getName();
    }
}
