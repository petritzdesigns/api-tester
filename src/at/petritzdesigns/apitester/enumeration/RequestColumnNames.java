package at.petritzdesigns.apitester.enumeration;

import at.petritzdesigns.apitester.model.RequestTableModel;

/**
 * Enum for Column Names used in RequestTableModel
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 * @see RequestTableModel
 */
public enum RequestColumnNames {
    NAME("Name"),
    URL("Url"),
    PARAMETER("Parameter");

    /**
     * User-friendly name of column
     */
    private final String name;

    /**
     * Default Constructor
     *
     * @param name the Name
     */
    private RequestColumnNames(String name) {
        this.name = name;
    }

    /**
     * Returns Name of Enum
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * ToString Method returns name
     *
     * @return name
     */
    @Override
    public String toString() {
        return getName();
    }
}
