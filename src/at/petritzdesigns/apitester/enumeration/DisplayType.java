package at.petritzdesigns.apitester.enumeration;

/**
 * DisplayType Enum used in ConfigurationDialog
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public enum DisplayType {
    SAVE_FILE,
    OPEN_FILE,
    INPUT_TEXT
}
