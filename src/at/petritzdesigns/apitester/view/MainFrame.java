package at.petritzdesigns.apitester.view;

import at.petritzdesigns.apitester.api.Parameter;
import at.petritzdesigns.apitester.api.Parameters;
import at.petritzdesigns.apitester.api.Request;
import at.petritzdesigns.apitester.enumeration.ConfigItem;
import at.petritzdesigns.apitester.model.ParameterTableModel;
import at.petritzdesigns.apitester.model.RequestComboBoxModel;
import at.petritzdesigns.apitester.model.RequestTableModel;
import at.petritzdesigns.apitester.util.Config;
import at.petritzdesigns.apitester.util.FileCreator;
import at.petritzdesigns.apitester.util.Ipv4AdressHelper;
import at.petritzdesigns.apitester.util.Logger;
import at.petritzdesigns.apitester.util.MD5Hasher;
import at.petritzdesigns.apitester.util.RequestOutputWriter;
import at.petritzdesigns.apitester.util.Status;
import at.petritzdesigns.apitester.util.XMLHelper;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JFileChooser;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

/**
 * Main Frame of API Tester
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class MainFrame extends javax.swing.JFrame {

    /**
     * TableModel for Parameter
     */
    private ParameterTableModel parameterModel;

    /**
     * Table Model for Request
     */
    private RequestTableModel savedRequestsModel;

    /**
     * Combobox Model for Request
     */
    private RequestComboBoxModel requestModel;

    /**
     * Executor Service to call heavy operations in a new thread
     */
    private ExecutorService service;

    /**
     * Default Constructor<br>
     * initializes everything
     */
    public MainFrame() {
        initComponents();
        initListeners();
        initThreads();
        initFrame();
        initModel();
        initOutputArea();
        endThreads();
    }

    /**
     * Initialize row selected Listener for parameters table
     */
    private void initListeners() {
        ListSelectionModel selectionModel = tParameters.getSelectionModel();
        selectionModel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        selectionModel.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                updateRemoveButton();
            }
        });
    }

    /**
     * Initialize Executor Service
     */
    private void initThreads() {
        service = Executors.newFixedThreadPool(10);
    }

    /**
     * Initialize Frame Position and Status
     */
    private void initFrame() {
        setLocationRelativeTo(null);
        Status.getInstance().setLabel(lbStatus);
        tfUrl.requestFocus();
    }

    /**
     * Initialize Models
     */
    private void initModel() {
        parameterModel = new ParameterTableModel();
        requestModel = new RequestComboBoxModel();
        savedRequestsModel = new RequestTableModel();
        updateModel();
        updateRemoveButton();
    }

    /**
     * Initialize Output Area (Syntax Highlighting)
     */
    private void initOutputArea() {
        staHttpGetOutput.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
        staHttpGetOutput.setCodeFoldingEnabled(true);
    }

    /**
     * Set Models and read data
     */
    private void updateModel() {
        if (parameterModel != null) {
            tParameters.setModel(parameterModel);
        }
        if (requestModel != null) {
            cbSavedRequests.setModel(requestModel);
            service.submit(new Runnable() {
                @Override
                public void run() {
                    requestModel.readRequests();
                    if (cbSavedRequests.getItemCount() > 0) {
                        cbSavedRequests.setSelectedIndex(0);
                    }
                }
            });
        }
        if (savedRequestsModel != null) {
            tSavedRequests.setModel(savedRequestsModel);
            service.submit(new Runnable() {
                @Override
                public void run() {
                    savedRequestsModel.readRequests();
                }
            });
        }
    }

    /**
     * Execute threads
     */
    private void endThreads() {
        service.shutdown();
    }

    /**
     * If a row is selected the remove button is enabled otherwise it is
     * disabled
     */
    private void updateRemoveButton() {
        btRemove.setEnabled(tParameters.getSelectedRowCount() != 0);
    }

    /**
     * Reload requests (combobox and table model)<br>
     * Sets first item of combobox if present
     */
    private void reloadRequests() {
        initThreads();
        service.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    requestModel.readRequests();
                    savedRequestsModel.readRequests();
                    if (cbSavedRequests.getItemCount() > 0) {
                        cbSavedRequests.setSelectedIndex(0);
                    } else {
                        cbSavedRequests.setSelectedIndex(-1);
                    }
                } catch (Exception ex) {
                    Logger.v(null, ex);
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnHeader = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        pnFooter = new javax.swing.JPanel();
        lbStatus = new javax.swing.JLabel();
        pnMethods = new javax.swing.JPanel();
        tpMethods = new javax.swing.JTabbedPane();
        pnHttpGet = new javax.swing.JPanel();
        spHttpGet = new javax.swing.JSplitPane();
        pnHttpGetParameters = new javax.swing.JPanel();
        lbRequestTitle = new javax.swing.JLabel();
        lbParametersTitle = new javax.swing.JLabel();
        lbSaveTitle = new javax.swing.JLabel();
        tfSaveName = new javax.swing.JTextField();
        btSave = new javax.swing.JButton();
        seperator = new javax.swing.JSeparator();
        lbSavedRequestsTitle = new javax.swing.JLabel();
        cbSavedRequests = new javax.swing.JComboBox();
        btApply = new javax.swing.JButton();
        lbUrlTitle = new javax.swing.JLabel();
        tfUrl = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        btHttpGetMakeRequest = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tParameters = new javax.swing.JTable();
        pnKeyValue = new javax.swing.JPanel();
        lbKeyTitle = new javax.swing.JLabel();
        lbKeyValue = new javax.swing.JLabel();
        tfKey = new javax.swing.JTextField();
        tfValue = new javax.swing.JTextField();
        pnOperator = new javax.swing.JPanel();
        btAdd = new javax.swing.JButton();
        btRemove = new javax.swing.JButton();
        pnHttpGetOutput = new javax.swing.JPanel();
        tbHttpGetOutputToolbar = new javax.swing.JToolBar();
        btConfig = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 0));
        btOutputSave = new javax.swing.JButton();
        tspHttpGetOutput = new org.fife.ui.rtextarea.RTextScrollPane();
        staHttpGetOutput = new org.fife.ui.rsyntaxtextarea.RSyntaxTextArea();
        pnUtilility = new javax.swing.JPanel();
        pnMd5 = new javax.swing.JPanel();
        lbMd5Input = new javax.swing.JLabel();
        tfMd5Input = new javax.swing.JTextField();
        lbMd5Output = new javax.swing.JLabel();
        tfMd5Output = new javax.swing.JTextField();
        btToParameter = new javax.swing.JButton();
        pnSavedRequests = new javax.swing.JPanel();
        pnSavedRequestsToolbar = new javax.swing.JPanel();
        btRemoveSavedRequest = new javax.swing.JButton();
        spSavedRequests = new javax.swing.JScrollPane();
        tSavedRequests = new javax.swing.JTable();
        mbMain = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        menuItemConfig = new javax.swing.JMenuItem();
        spFile = new javax.swing.JPopupMenu.Separator();
        menuItemQuit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("API Tester");
        setMinimumSize(new java.awt.Dimension(600, 950));
        setPreferredSize(new java.awt.Dimension(1000, 950));

        pnHeader.setLayout(new java.awt.GridBagLayout());

        lbTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 24)); // NOI18N
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("API Tester");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 10, 0, 10);
        pnHeader.add(lbTitle, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("for web (c) Markus Petritz");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 5, 10);
        pnHeader.add(jLabel1, gridBagConstraints);

        getContentPane().add(pnHeader, java.awt.BorderLayout.PAGE_START);

        pnFooter.setLayout(new java.awt.GridBagLayout());

        lbStatus.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbStatus.setText("Running");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnFooter.add(lbStatus, gridBagConstraints);

        getContentPane().add(pnFooter, java.awt.BorderLayout.PAGE_END);

        pnMethods.setLayout(new java.awt.GridLayout(1, 0));

        pnHttpGet.setLayout(new java.awt.GridLayout(1, 0));

        spHttpGet.setDividerLocation(460);

        pnHttpGetParameters.setMinimumSize(new java.awt.Dimension(454, 713));
        pnHttpGetParameters.setLayout(new java.awt.GridBagLayout());

        lbRequestTitle.setFont(new java.awt.Font("Helvetica Neue", 1, 18)); // NOI18N
        lbRequestTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbRequestTitle.setText("New Request");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 10, 2);
        pnHttpGetParameters.add(lbRequestTitle, gridBagConstraints);

        lbParametersTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbParametersTitle.setText("Parameters");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(15, 3, 2, 2);
        pnHttpGetParameters.add(lbParametersTitle, gridBagConstraints);

        lbSaveTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbSaveTitle.setText("Save");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(lbSaveTitle, gridBagConstraints);

        tfSaveName.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfSaveName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSave(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(tfSaveName, gridBagConstraints);

        btSave.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        btSave.setText("Save");
        btSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSave(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(btSave, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(seperator, gridBagConstraints);

        lbSavedRequestsTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbSavedRequestsTitle.setText("Saved Requests");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(lbSavedRequestsTitle, gridBagConstraints);

        cbSavedRequests.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        cbSavedRequests.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(cbSavedRequests, gridBagConstraints);

        btApply.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        btApply.setText("Apply");
        btApply.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onApply(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(btApply, gridBagConstraints);

        lbUrlTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbUrlTitle.setText("Url");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 7, 2, 2);
        pnHttpGetParameters.add(lbUrlTitle, gridBagConstraints);

        tfUrl.setColumns(18);
        tfUrl.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfUrl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onMakeRequest(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(tfUrl, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(jSeparator1, gridBagConstraints);

        btHttpGetMakeRequest.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        btHttpGetMakeRequest.setText("Make Request");
        btHttpGetMakeRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onMakeRequest(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipady = 4;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnHttpGetParameters.add(btHttpGetMakeRequest, gridBagConstraints);

        tParameters.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tParameters);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        pnHttpGetParameters.add(jScrollPane1, gridBagConstraints);

        pnKeyValue.setLayout(new java.awt.GridLayout(2, 2));

        lbKeyTitle.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbKeyTitle.setText("Key");
        pnKeyValue.add(lbKeyTitle);

        lbKeyValue.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbKeyValue.setText("Value");
        pnKeyValue.add(lbKeyValue);

        tfKey.setColumns(9);
        tfKey.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onParameterEntered(evt);
            }
        });
        pnKeyValue.add(tfKey);

        tfValue.setColumns(9);
        tfValue.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfValue.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onParameterEntered(evt);
            }
        });
        pnKeyValue.add(tfValue);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnHttpGetParameters.add(pnKeyValue, gridBagConstraints);

        pnOperator.setLayout(new java.awt.GridLayout(1, 0));

        btAdd.setFont(new java.awt.Font("Helvetica Neue", 0, 18)); // NOI18N
        btAdd.setText("+");
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onAdd(evt);
            }
        });
        pnOperator.add(btAdd);

        btRemove.setFont(new java.awt.Font("Helvetica Neue", 0, 18)); // NOI18N
        btRemove.setText("-");
        btRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onRemove(evt);
            }
        });
        pnOperator.add(btRemove);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        pnHttpGetParameters.add(pnOperator, gridBagConstraints);

        spHttpGet.setLeftComponent(pnHttpGetParameters);

        pnHttpGetOutput.setLayout(new java.awt.BorderLayout());

        tbHttpGetOutputToolbar.setRollover(true);

        btConfig.setFont(new java.awt.Font("Helvetica Neue", 0, 11)); // NOI18N
        btConfig.setText("Configuration");
        btConfig.setFocusable(false);
        btConfig.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btConfig.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onConfig(evt);
            }
        });
        tbHttpGetOutputToolbar.add(btConfig);
        tbHttpGetOutputToolbar.add(filler1);

        btOutputSave.setFont(new java.awt.Font("Helvetica Neue", 0, 11)); // NOI18N
        btOutputSave.setText("Save Output");
        btOutputSave.setFocusable(false);
        btOutputSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btOutputSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btOutputSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onOutputSave(evt);
            }
        });
        tbHttpGetOutputToolbar.add(btOutputSave);

        pnHttpGetOutput.add(tbHttpGetOutputToolbar, java.awt.BorderLayout.PAGE_END);

        staHttpGetOutput.setColumns(20);
        staHttpGetOutput.setRows(5);
        tspHttpGetOutput.setViewportView(staHttpGetOutput);

        pnHttpGetOutput.add(tspHttpGetOutput, java.awt.BorderLayout.CENTER);

        spHttpGet.setRightComponent(pnHttpGetOutput);

        pnHttpGet.add(spHttpGet);

        tpMethods.addTab("HTTP GET", pnHttpGet);

        pnUtilility.setLayout(new java.awt.BorderLayout());

        pnMd5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MD5 Hasher", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Helvetica Neue", 0, 13))); // NOI18N
        pnMd5.setLayout(new java.awt.GridBagLayout());

        lbMd5Input.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbMd5Input.setText("Input");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMd5.add(lbMd5Input, gridBagConstraints);

        tfMd5Input.setColumns(30);
        tfMd5Input.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfMd5Input.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                onMd5InputKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMd5.add(tfMd5Input, gridBagConstraints);

        lbMd5Output.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        lbMd5Output.setText("Output");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMd5.add(lbMd5Output, gridBagConstraints);

        tfMd5Output.setEditable(false);
        tfMd5Output.setColumns(30);
        tfMd5Output.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        tfMd5Output.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMd5.add(tfMd5Output, gridBagConstraints);

        btToParameter.setFont(new java.awt.Font("Helvetica Neue", 0, 14)); // NOI18N
        btToParameter.setText("To Parameter");
        btToParameter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onMd5ToParameter(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnMd5.add(btToParameter, gridBagConstraints);

        pnUtilility.add(pnMd5, java.awt.BorderLayout.NORTH);

        pnSavedRequests.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Saved Requests", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Helvetica Neue", 0, 13))); // NOI18N
        pnSavedRequests.setLayout(new java.awt.BorderLayout());

        btRemoveSavedRequest.setText("Remove");
        btRemoveSavedRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onRemoveSavedRequest(evt);
            }
        });
        pnSavedRequestsToolbar.add(btRemoveSavedRequest);

        pnSavedRequests.add(pnSavedRequestsToolbar, java.awt.BorderLayout.PAGE_START);

        tSavedRequests.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        spSavedRequests.setViewportView(tSavedRequests);

        pnSavedRequests.add(spSavedRequests, java.awt.BorderLayout.CENTER);

        pnUtilility.add(pnSavedRequests, java.awt.BorderLayout.CENTER);

        tpMethods.addTab("Utility", pnUtilility);

        pnMethods.add(tpMethods);

        getContentPane().add(pnMethods, java.awt.BorderLayout.CENTER);

        menuFile.setText("File");

        menuItemConfig.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        menuItemConfig.setText("Configuration");
        menuItemConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onConfig(evt);
            }
        });
        menuFile.add(menuItemConfig);
        menuFile.add(spFile);

        menuItemQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        menuItemQuit.setText("Exit");
        menuItemQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onQuit(evt);
            }
        });
        menuFile.add(menuItemQuit);

        mbMain.add(menuFile);

        setJMenuBar(mbMain);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Action Event for "+" button<br>
     * Adds parameter to model
     *
     * @param evt
     */
    private void onAdd(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onAdd
        try {
            String key = tfKey.getText().trim();
            String value = tfValue.getText().trim();

            if (key.isEmpty()) {
                tfKey.requestFocus();
                throw new Exception("Please enter a valid key.");
            }

            if (value.isEmpty()) {
                tfValue.requestFocus();
                throw new Exception("Please enter a valid value.");
            }

            Parameter p = new Parameter(key, value);
            parameterModel.add(p);

            tfKey.setText("");
            tfValue.setText("");
            tfKey.requestFocus();

        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onAdd

    /**
     * Action Event for "save" button<br>
     * Writes request to file
     *
     * @see RequestOutputWriter
     * @param evt
     */
    private void onSave(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSave
        try {
            String name = tfSaveName.getText();
            String url = tfUrl.getText();
            Parameters params = parameterModel.getParameters();

            if (name.isEmpty()) {
                tfSaveName.requestFocus();
                throw new Exception("Please enter a valid name.");
            }

            if (url.isEmpty()) {
                tfUrl.requestFocus();
                throw new Exception("Please enter a valid url.");
            }

            Request request = new Request(new URL(url), params);
            request.setName(name);

            String file = Config.getDefault().get(ConfigItem.DATA_PATH.getName());
            FileCreator.createFileIfNotExist(file);
            RequestOutputWriter row = new RequestOutputWriter(file);

            row.append(request);

            tfSaveName.setText("");
            tfUrl.requestFocus();

            reloadRequests();
        } catch (Exception ex) {
            Logger.v(this, ex);
            Logger.w(ex);
        }
    }//GEN-LAST:event_onSave

    /**
     * Action Event for "apply" button<br>
     * Loads selected request and updates textfields
     *
     * @param evt
     */
    private void onApply(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onApply
        try {

            Object obj = cbSavedRequests.getSelectedItem();
            if (obj == null) {
                cbSavedRequests.requestFocus();
                throw new Exception("No Item selected.");
            }
            if (!(obj instanceof Request)) {
                throw new Exception("Strange error.");
            }
            Request request = (Request) obj;
            tfUrl.setText(request.getUrl().toString());
            parameterModel = new ParameterTableModel(request.getParameter());
            tParameters.setModel(parameterModel);

        } catch (Exception ex) {
            Logger.v(this, ex);
            Logger.w(ex);
        }
    }//GEN-LAST:event_onApply

    /**
     * Action Event for "Make Request" button<br>
     * Makes new request and calls fire method in a new thread, the output is
     * then displayed as formatted xml in the textarea.
     *
     * @param evt
     */
    private void onMakeRequest(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onMakeRequest
        try {
            String url = tfUrl.getText().trim();

            if (url.isEmpty() || Ipv4AdressHelper.validate(url)) {
                tfUrl.requestFocus();
                throw new Exception("Please enter a valid url.");
            }

            initThreads();
            service.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        Request request = new Request(new URL(url), parameterModel.getParameters());
                        String output = request.fire();
                        staHttpGetOutput.setText(XMLHelper.format(output));
                    } catch (Exception ex) {
                        staHttpGetOutput.setText(ex.toString());
                    }
                }
            });
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onMakeRequest

    /**
     * Action Event when user types something in the md5 input textfield<br>
     * creates hash of input and shows in the md5 output textfield.
     *
     * @see MD5Hasher
     * @param evt
     */
    private void onMd5InputKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_onMd5InputKeyReleased
        try {
            String input = tfMd5Input.getText();
            tfMd5Output.setText(MD5Hasher.create(input));
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onMd5InputKeyReleased

    /**
     * Action Event for the "To Parameter" button<br>
     * Adds MD5 Output to the Value textfield for the request
     *
     * @param evt
     */
    private void onMd5ToParameter(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onMd5ToParameter
        if (tfMd5Output.getText().isEmpty()) {
            onMd5InputKeyReleased(null);
        }
        String output = tfMd5Output.getText();
        tpMethods.setSelectedIndex(0); //Goto HTTP Get Pane
        tfValue.setText(output);
        tfKey.requestFocus();
    }//GEN-LAST:event_onMd5ToParameter

    /**
     * Action Event when user "entered" a textfield (key, value)
     *
     * @param evt
     */
    private void onParameterEntered(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onParameterEntered
        if (tfKey.getText().isEmpty()) {
            tfKey.requestFocus();
        } else if (tfValue.getText().isEmpty()) {
            tfValue.requestFocus();
        } else {
            onAdd(evt);
        }
    }//GEN-LAST:event_onParameterEntered

    /**
     * Action Event when the "-" button is pressed<br>
     * removes selected parameter(s) from the model
     *
     * @param evt
     */
    private void onRemove(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onRemove
        try {
            int[] rows = tParameters.getSelectedRows();
            if (rows.length == 0) {
                tParameters.requestFocus();
                throw new Exception("You have to select a row to remove.");
            }

            parameterModel.remove(rows);

        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onRemove

    /**
     * Action Event for the "save" button<br>
     * Saves output in the textarea to a file the user chooses (JFileChooser)
     *
     * @param evt
     */
    private void onOutputSave(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onOutputSave
        try {
            String text = staHttpGetOutput.getText();
            JFileChooser fc = new JFileChooser(System.getProperty("user.home"));
            int ret = fc.showSaveDialog(this);
            if (ret == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(file));
                bw.write(text);
                bw.flush();
                bw.close();
                Status.getInstance().write("Saved Output to: " + file.getAbsolutePath());
            }
        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onOutputSave

    /**
     * Action Event for the "quit" menu item<br>
     * quits the application completely
     *
     * @param evt
     */
    private void onQuit(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onQuit
        //Update things
        System.exit(0);
    }//GEN-LAST:event_onQuit

    /**
     * Action Event for the "config" menu item<br>
     * shows Configuration Dialog and reloads requests afterwards
     *
     * @see ConfigurationDialog
     * @param evt
     */
    private void onConfig(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onConfig
        new ConfigurationDialog(this, true).setVisible(true);
        reloadRequests();
    }//GEN-LAST:event_onConfig

    /**
     * Action Event for the "remove" button in the saved requests panel<br>
     * REmoves selected row(s) from the model
     *
     * @param evt
     */
    private void onRemoveSavedRequest(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onRemoveSavedRequest
        try {
            int[] rows = tSavedRequests.getSelectedRows();
            if (rows.length == 0) {
                tSavedRequests.requestFocus();
                throw new Exception("You have to select a row to remove.");
            }

            savedRequestsModel.remove(rows);
            reloadRequests();

        } catch (Exception ex) {
            Logger.v(this, ex);
        }
    }//GEN-LAST:event_onRemoveSavedRequest

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btApply;
    private javax.swing.JButton btConfig;
    private javax.swing.JButton btHttpGetMakeRequest;
    private javax.swing.JButton btOutputSave;
    private javax.swing.JButton btRemove;
    private javax.swing.JButton btRemoveSavedRequest;
    private javax.swing.JButton btSave;
    private javax.swing.JButton btToParameter;
    private javax.swing.JComboBox cbSavedRequests;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbKeyTitle;
    private javax.swing.JLabel lbKeyValue;
    private javax.swing.JLabel lbMd5Input;
    private javax.swing.JLabel lbMd5Output;
    private javax.swing.JLabel lbParametersTitle;
    private javax.swing.JLabel lbRequestTitle;
    private javax.swing.JLabel lbSaveTitle;
    private javax.swing.JLabel lbSavedRequestsTitle;
    private javax.swing.JLabel lbStatus;
    private javax.swing.JLabel lbTitle;
    private javax.swing.JLabel lbUrlTitle;
    private javax.swing.JMenuBar mbMain;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenuItem menuItemConfig;
    private javax.swing.JMenuItem menuItemQuit;
    private javax.swing.JPanel pnFooter;
    private javax.swing.JPanel pnHeader;
    private javax.swing.JPanel pnHttpGet;
    private javax.swing.JPanel pnHttpGetOutput;
    private javax.swing.JPanel pnHttpGetParameters;
    private javax.swing.JPanel pnKeyValue;
    private javax.swing.JPanel pnMd5;
    private javax.swing.JPanel pnMethods;
    private javax.swing.JPanel pnOperator;
    private javax.swing.JPanel pnSavedRequests;
    private javax.swing.JPanel pnSavedRequestsToolbar;
    private javax.swing.JPanel pnUtilility;
    private javax.swing.JSeparator seperator;
    private javax.swing.JPopupMenu.Separator spFile;
    private javax.swing.JSplitPane spHttpGet;
    private javax.swing.JScrollPane spSavedRequests;
    private org.fife.ui.rsyntaxtextarea.RSyntaxTextArea staHttpGetOutput;
    private javax.swing.JTable tParameters;
    private javax.swing.JTable tSavedRequests;
    private javax.swing.JToolBar tbHttpGetOutputToolbar;
    private javax.swing.JTextField tfKey;
    private javax.swing.JTextField tfMd5Input;
    private javax.swing.JTextField tfMd5Output;
    private javax.swing.JTextField tfSaveName;
    private javax.swing.JTextField tfUrl;
    private javax.swing.JTextField tfValue;
    private javax.swing.JTabbedPane tpMethods;
    private org.fife.ui.rtextarea.RTextScrollPane tspHttpGetOutput;
    // End of variables declaration//GEN-END:variables

}
