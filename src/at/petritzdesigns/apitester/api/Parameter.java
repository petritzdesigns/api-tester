package at.petritzdesigns.apitester.api;

import java.io.Serializable;

/**
 * Parameter Data Class. Holds 2 Attributes, a Key and a Value.
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Parameter implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 17472128206291L;

    /**
     * Key Attribute
     */
    private final String key;

    /**
     * Value Attribute
     */
    private final String value;

    /**
     * Default Constructor
     *
     * @param key the Key
     * @param value the Value
     */
    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * Gets the Key
     *
     * @return key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the Value
     *
     * @return value
     */
    public String getValue() {
        return value;
    }

    /**
     * ToString Method format: key=value
     *
     * @return key=value
     */
    @Override
    public String toString() {
        return String.format("%s=%s", key, value);
    }
}
