package at.petritzdesigns.apitester.api;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Parameters Class holds a Map with Parameter. Used to build queries.
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Parameters implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 17472128206292L;

    /**
     * Map that holds Parameter the Key of the Map is also the Key of the
     * Parameter
     */
    private final Map<String, Parameter> params;

    /**
     * Default Constructor constructs an empty map
     */
    public Parameters() {
        this.params = new HashMap<>();
    }

    /**
     * Constructor that fills map with the given collection
     *
     * @param collection Collection of Parameter
     */
    public Parameters(Collection<Parameter> collection) {
        this.params = new HashMap<>();
        for (Parameter parameter : collection) {
            this.params.put(parameter.getKey(), parameter);
        }
    }

    /**
     * Returns Map as a Set of Parameter
     *
     * @return set
     */
    public Set<Parameter> asSet() {
        Set<Parameter> set = new HashSet<>();
        for (Map.Entry<String, Parameter> entry : params.entrySet()) {
            set.add(entry.getValue());
        }
        return set;
    }

    /**
     * Returns Map
     *
     * @return map
     */
    public Map<String, Parameter> asMap() {
        return params;
    }

    /**
     * Creates a query of all elements of the map
     *
     * @return query string
     */
    public String queryString() {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (Map.Entry<String, Parameter> entry : params.entrySet()) {
            if (isFirst) {
                sb.append(String.format("?%s", entry.getValue().toString()));
                isFirst = false;
            } else {
                sb.append(String.format("&%s", entry.getValue().toString()));
            }
        }
        return sb.toString();
    }

    /**
     * ToString Method equivalent to queryString()
     *
     * @return query string
     */
    @Override
    public String toString() {
        return queryString();
    }

}
