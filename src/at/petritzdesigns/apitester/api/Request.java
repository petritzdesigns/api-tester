package at.petritzdesigns.apitester.api;

import at.petritzdesigns.apitester.util.Logger;
import at.petritzdesigns.apitester.util.WebRequest;
import java.io.Serializable;
import java.net.URL;
import java.util.Objects;

/**
 * Request Class holds the Name, the URL and the Parameters.
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Request implements Serializable {

    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = 17472128206293L;

    /**
     * Name of the Request
     */
    private String name;

    /**
     * URL of the Request
     */
    private final URL url;

    /**
     * Parameters of the Request
     *
     * @see Parameters
     */
    private final Parameters params;

    /**
     * Default Constructor, sets Name to "Undefined"
     *
     * @param url the URL
     * @param params the Parameters
     */
    public Request(URL url, Parameters params) {
        this.name = "Undefined";
        this.url = url;
        this.params = params;
    }

    /**
     * Constructor
     *
     * @param name the Name
     * @param url the URL
     * @param params the Parameters
     */
    public Request(String name, URL url, Parameters params) {
        this.name = name;
        this.url = url;
        this.params = params;
    }

    /**
     * Sets the Name of the Request
     *
     * @param name the Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the Name of the Request
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the URL of the Request
     *
     * @return
     */
    public URL getUrl() {
        return url;
    }

    /**
     * Returns the Parameters of the Request
     *
     * @return
     */
    public Parameters getParameter() {
        return params;
    }

    /**
     * ToString method returns name of the Request
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Generated HashCode Method
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.url);
        hash = 37 * hash + Objects.hashCode(this.params);
        return hash;
    }

    /**
     * Generated Equals Method
     *
     * @param obj Object to compare
     * @return true if values are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Request other = (Request) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.url, other.url)) {
            return false;
        }
        if (!Objects.equals(this.params, other.params)) {
            return false;
        }
        return true;
    }

    /**
     * Synchronous Method to Fire a Request
     *
     * @return response
     * @see WebRequest
     */
    public String fire() {
        //synchronous
        try {
            String newUrl = url.toString();
            if (url.toString().endsWith("?")) {
                newUrl = newUrl.concat(params.toString().replace("?", "&"));
            } else {
                newUrl = newUrl.concat(params.toString());
            }
            URL connUrl = new URL(newUrl);
            return WebRequest.make(connUrl);

        } catch (Exception e) {
            Logger.e(e);
            return null;
        }
    }
}
