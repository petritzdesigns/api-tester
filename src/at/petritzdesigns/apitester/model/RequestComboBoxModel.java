package at.petritzdesigns.apitester.model;

import at.petritzdesigns.apitester.api.Request;
import at.petritzdesigns.apitester.enumeration.ConfigItem;
import at.petritzdesigns.apitester.util.Config;
import at.petritzdesigns.apitester.util.FileCreator;
import at.petritzdesigns.apitester.util.Logger;
import at.petritzdesigns.apitester.util.RequestReader;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;

/**
 * ComboBoxModel for Requests
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class RequestComboBoxModel extends DefaultComboBoxModel<Request> {

    /**
     * List of Request
     */
    private List<Request> list;

    /**
     * Default Constructor<br>
     * creates empty list
     */
    public RequestComboBoxModel() {
        this.list = new LinkedList<>();
    }

    /**
     * Read Requests from file (config) and adds them to the list (fires
     * interval added)
     */
    public void readRequests() {
        try {
            String file = Config.getDefault().get(ConfigItem.DATA_PATH.getName());
            FileCreator.createFileIfNotExist(file);
            RequestReader reader = new RequestReader(file);
            List<Request> temp = reader.readAll();
            if (temp != null) {
                list = temp;
            } else {
                list = new LinkedList<>();
            }
        } catch (Exception ex) {
            Logger.e(ex);
        }

        super.fireIntervalAdded(this, 0, list.isEmpty() ? 0 : list.size() - 1);
    }

    /**
     * Adds Request to list (fires interval added)
     *
     * @param request Request to add
     */
    public void addRequest(Request request) {
        list.add(request);
        super.fireIntervalAdded(this, 0, list.isEmpty() ? 0 : list.size() - 1);
    }

    /**
     * Gets index of given element
     *
     * @param anObject element
     * @return index
     */
    @Override
    public int getIndexOf(Object anObject) {
        return list.indexOf(anObject);
    }

    /**
     * Gets element at given index
     *
     * @param index index
     * @return element
     */
    @Override
    public Request getElementAt(int index) {
        return list.get(index);
    }

    /**
     * Returns size of list
     *
     * @return size
     */
    @Override
    public int getSize() {
        return list.size();
    }

}
