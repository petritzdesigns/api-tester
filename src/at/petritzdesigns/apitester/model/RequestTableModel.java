package at.petritzdesigns.apitester.model;

import at.petritzdesigns.apitester.api.Request;
import at.petritzdesigns.apitester.enumeration.ConfigItem;
import at.petritzdesigns.apitester.enumeration.RequestColumnNames;
import at.petritzdesigns.apitester.util.Config;
import at.petritzdesigns.apitester.util.FileCreator;
import at.petritzdesigns.apitester.util.Logger;
import at.petritzdesigns.apitester.util.RequestOutputWriter;
import at.petritzdesigns.apitester.util.RequestReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * TableModel for Requests
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class RequestTableModel extends AbstractTableModel {

    /**
     * List of Request
     */
    private List<Request> list;

    /**
     * Default Constructor<br>
     * makes an empty list
     */
    public RequestTableModel() {
        list = new LinkedList<>();
    }

    /**
     * Reads Request from file (config) and adds them to the list (fires table
     * data changed)
     */
    public void readRequests() {
        try {
            String file = Config.getDefault().get(ConfigItem.DATA_PATH.getName());
            FileCreator.createFileIfNotExist(file);
            RequestReader reader = new RequestReader(file);
            List<Request> temp = reader.readAll();
            if (temp != null) {
                list = temp;
            } else {
                list = new LinkedList<>();
            }
        } catch (Exception ex) {
            Logger.e(ex);
        }

        super.fireTableDataChanged();
    }

    /**
     * Removes given indices from list and writes them to file (config) (fires
     * table data changed)
     *
     * @param rows rows to be deleted
     * @throws IOException
     */
    public void remove(int[] rows) throws IOException {
        List<Integer> temp = new LinkedList<>();
        for (int row : rows) {
            temp.add(row);
        }
        temp.sort(new DescendingComparator());
        for (Integer integer : temp) {
            list.remove((int) integer);
        }
        RequestOutputWriter row = new RequestOutputWriter(Config.getDefault().get(ConfigItem.DATA_PATH.getName()));
        row.write(list);
        super.fireTableDataChanged();
    }

    /**
     * Returns list size
     *
     * @return row count
     */
    @Override
    public int getRowCount() {
        return list.size();
    }

    /**
     * Return RequestColumnNames size
     *
     * @return columnt count
     * @see RequestColumnNames
     */
    @Override
    public int getColumnCount() {
        return RequestColumnNames.values().length;
    }

    /**
     * Return Column name
     *
     * @param column col
     * @return name
     * @see RequestColumnNames
     */
    @Override
    public String getColumnName(int column) {
        return RequestColumnNames.values()[column].getName();
    }

    /**
     * Return Value at given row index and column index
     *
     * @param rowIndex row index
     * @param columnIndex col index
     * @return value
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Request r = list.get(rowIndex);
        switch (RequestColumnNames.values()[columnIndex]) {
            case NAME:
                return r.getName();
            case PARAMETER:
                return r.getParameter().queryString();
            case URL:
                return r.getUrl().toExternalForm();
            default:
                return "-";
        }
    }

}
