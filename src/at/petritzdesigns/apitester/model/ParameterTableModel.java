package at.petritzdesigns.apitester.model;

import at.petritzdesigns.apitester.api.Parameter;
import at.petritzdesigns.apitester.api.Parameters;
import at.petritzdesigns.apitester.enumeration.ParameterColumnNames;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * TableModel for Parameters
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class ParameterTableModel extends AbstractTableModel {

    /**
     * List of Parameter
     */
    private final List<Parameter> list;

    /**
     * Default Constructor creates an empty list
     */
    public ParameterTableModel() {
        this.list = new LinkedList<>();
    }

    /**
     * Constructor reads params from Parameters and adds them to the list
     *
     * @param params parameter
     */
    public ParameterTableModel(Parameters params) {
        this.list = new LinkedList<>(params.asSet());
    }

    /**
     * Add Parameter to list (fires table data changed)
     *
     * @param param parameter to be added
     */
    public void add(Parameter param) {
        list.add(param);
        super.fireTableDataChanged();
    }

    /**
     * Removes given index from the list (fires table data changed)
     *
     * @param index index to remove
     */
    public void remove(int index) {
        list.remove(index);
        super.fireTableDataChanged();
    }

    /**
     * Removes given indices from the list (fires table data changed)<br>
     * Sorts Elements in descending order before they are removed
     *
     * @param rows rows to delete
     */
    public void remove(int[] rows) {
        List<Integer> temp = new LinkedList<>();
        for (int row : rows) {
            temp.add(row);
        }
        temp.sort(new DescendingComparator());
        for (Integer integer : temp) {
            list.remove((int) integer);
        }
        super.fireTableDataChanged();
    }

    /**
     * Returns list as Parameters
     *
     * @return parameters
     */
    public Parameters getParameters() {
        return new Parameters(list);
    }

    /**
     * Returns size of list
     *
     * @return row count
     */
    @Override
    public int getRowCount() {
        return list.size();
    }

    /**
     * Returns size of ParameterColumnNames Enum
     *
     * @return column count
     * @see ParameterColumnNames
     */
    @Override
    public int getColumnCount() {
        return ParameterColumnNames.values().length;
    }

    /**
     * Returns name of column
     *
     * @param column col
     * @return name
     * @see ParameterColumnNames
     */
    @Override
    public String getColumnName(int column) {
        return ParameterColumnNames.values()[column].getName();
    }

    /**
     * Returns if cell should be editable
     *
     * @param rowIndex row
     * @param columnIndex col
     * @return always true
     */
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    /**
     * Returns Object from Parameter at given row and column index
     *
     * @param rowIndex row
     * @param columnIndex col
     * @return Object
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Parameter param = list.get(rowIndex);
        switch (ParameterColumnNames.values()[columnIndex]) {
            case KEY:
                return param.getKey();
            case VALUE:
                return param.getValue();
            default:
                return "-";
        }
    }

    /**
     * Makes a new Object of the given value and adds it to the list
     *
     * @param aValue value
     * @param rowIndex row
     * @param columnIndex col
     */
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String val = aValue.toString();
        Parameter old = list.get(rowIndex);

        switch (ParameterColumnNames.values()[columnIndex]) {
            case KEY:
                list.set(rowIndex, new Parameter(val, old.getValue()));
                break;
            case VALUE:
                list.set(rowIndex, new Parameter(old.getKey(), val));
                break;
            default:
                break;
        }
    }

}
