package at.petritzdesigns.apitester.model;

import java.util.Comparator;

/**
 * Comparator to get Elements in descending order
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class DescendingComparator implements Comparator<Integer> {

    /**
     * Compare Method
     *
     * @param o1 first element
     * @param o2 second element
     * @return
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        return o2.compareTo(o1);
    }

}
