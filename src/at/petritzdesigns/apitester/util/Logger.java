package at.petritzdesigns.apitester.util;

import java.awt.Component;
import javax.swing.JOptionPane;

/**
 * Logger Singleton class
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Logger {

    /**
     * Instance needed for use with singletons
     */
    private static Logger instance;

    /**
     * Default Constructor
     */
    private Logger() {
    }

    /**
     * Creates and returns instance
     *
     * @return instance
     */
    public static Logger getInstance() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    /**
     * Debug message (short version)
     *
     * @param text to debug
     */
    public static void d(Object text) {
        getInstance().debug(text);
    }

    /**
     * Error message (short version)
     *
     * @param text to debug
     */
    public static void e(Object text) {
        getInstance().error(text);
    }

    /**
     * Warning message (short version)
     *
     * @param text to debug
     */
    public static void w(Object text) {
        getInstance().warning(text);
    }

    /**
     * Message to show in parent (short version)
     *
     * @param parent parent component
     * @param text to debug
     */
    public static void v(Component parent, Object text) {
        getInstance().view(parent, text);
    }

    /**
     * Debug Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void debug(Object text) {
        System.out.println("DEBUG: " + text.toString());
        Status.getInstance().write(text.toString());
    }

    /**
     * Error Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void error(Object text) {
        System.out.println("ERROR: " + text.toString());
        Status.getInstance().write("Error: " + text.toString());
    }

    /**
     * Warning Message (long version)<br>
     * Writes Status
     *
     * @see Status
     * @param text to debug
     */
    public void warning(Object text) {
        System.out.println("WARNING: " + text.toString());
        Status.getInstance().write("Warning: " + text.toString());
    }

    /**
     * Message to show in parent (long version)<br>
     * Writes Status and shows JOptionPane
     *
     * @see Status
     * @param parent parent component
     * @param text to debug
     */
    public void view(Component parent, Object text) {
        System.out.println("VIEW: " + text.toString());
        Status.getInstance().write("View: " + text.toString());
        JOptionPane.showMessageDialog(parent, text);
    }
}
