package at.petritzdesigns.apitester.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Web Request class to make Web requests
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class WebRequest {

    /**
     * Static initializer block to trust all certificates<br>
     * Taken from: http://stackoverflow.com/a/2893932
     */
    static {
        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (GeneralSecurityException e) {
        }
    }

    /**
     * Make request, auto determines if http or https (synchronous)<br>
     * writes Status
     *
     * @param url the Url
     * @return response
     * @throws IOException
     */
    public static String make(URL url) throws IOException {
        if (url.getProtocol().equals("http")) {
            return makeHttp(url);
        } else if (url.getProtocol().equals("https")) {
            return makeHttps(url);
        }
        return "";
    }

    /**
     * Make Http request (GET) (synchronous)<br>
     * writes Status
     *
     * @param url the Url
     * @return response
     * @throws IOException
     */
    public static String makeHttp(URL url) throws IOException {
        Status.getInstance().write("New http request: " + url.toString());

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET"); //GET only supported for now

        Status.getInstance().write("Response code: " + conn.getResponseCode());

        StringBuilder sb = new StringBuilder();
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            String line;
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        }
    }

    /**
     * Make Https request (GET) (synchronous)<br>
     * writes Status
     *
     * @param url the Url
     * @return response
     * @throws IOException
     */
    public static String makeHttps(URL url) throws IOException {
        Status.getInstance().write("New https request: " + url.toString());

        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("GET"); //GET only supported for now

        Status.getInstance().write("Response code: " + conn.getResponseCode());

        StringBuilder sb = new StringBuilder();
        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            String line;
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        }
    }
}
