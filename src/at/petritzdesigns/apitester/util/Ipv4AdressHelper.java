package at.petritzdesigns.apitester.util;

import java.util.regex.Pattern;

/**
 * Ipv4 Adress Helper utility clas
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Ipv4AdressHelper {

    /**
     * Regex to validate an IPv4 Adress
     */
    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    /**
     * Validates Ip adress
     *
     * @param ip ip adress to validate
     * @return true if valid
     */
    public static boolean validate(final String ip) {
        return PATTERN.matcher(ip).matches();
    }
}
