package at.petritzdesigns.apitester.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 Hasher utility class
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class MD5Hasher {

    /**
     * Hashes input with MD5 algorithm<br>
     * Taken From
     * <a href="http://www.mkyong.com/java/java-md5-hashing-example/">Mkyong</a>
     *
     * @param input input to hash
     * @return hashed input as hex
     * @throws NoSuchAlgorithmException if no MD5 hashing algorithm is
     * avaialable
     */
    public static String create(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(input.getBytes());

        byte byteData[] = md.digest();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
