package at.petritzdesigns.apitester.util;

import javax.swing.JLabel;

/**
 * Status singleton class<br>
 * writes status to label
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class Status {

    /**
     * Instance needed for singleton
     */
    private static Status instance;

    /**
     * Label to write status
     */
    private JLabel label;

    /**
     * Label has updated text
     */
    private boolean updated;

    /**
     * Last time label hast updated text
     */
    private long lastUpdate;

    /**
     * Thread for updates
     */
    private UpdaterThread thread;

    /**
     * Placeholder text if there is no active status
     */
    private static final String PLACEHOLDER = "Running.";

    /**
     * Default Constructor
     */
    private Status() {
        updated = false;
    }

    /**
     * Creates and returns instance
     *
     * @return instance
     */
    public static Status getInstance() {
        if (instance == null) {
            instance = new Status();
        }
        return instance;
    }

    /**
     * Set Label to write<br>
     * sets text to placeholder and runs new thread
     *
     * @param label the Label
     */
    public void setLabel(JLabel label) {
        this.label = label;
        this.label.setText(PLACEHOLDER);
        this.thread = new UpdaterThread();
        this.thread.start();
    }

    /**
     * Writes text to label
     *
     * @param text to write
     */
    public void write(String text) {
        if (label != null) {
            label.setText(text);
            updated = true;
            lastUpdate = System.currentTimeMillis();
        }
    }

    /**
     * Updater Thread<br>
     * status should disappear after 10 seconds
     */
    private class UpdaterThread extends Thread {

        /**
         * Run Method
         */
        @Override
        public void run() {
            while (label != null) {
                if (updated) {
                    if (lastUpdate < (System.currentTimeMillis() - TimeCreator.createSeconds(10))) {
                        label.setText(PLACEHOLDER);
                        updated = false;
                    }
                }
            }
        }

    }
}
