package at.petritzdesigns.apitester.util;

import at.petritzdesigns.apitester.api.Request;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

/**
 * Request Output Writer<br>
 * writes or appends request to given path
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class RequestOutputWriter {

    /**
     * Path to write/append request
     */
    private final String path;

    /**
     * Default Constructor
     *
     * @param path the Path
     */
    public RequestOutputWriter(String path) {
        this.path = path;
    }

    /**
     * Append given request to path<br>
     * First reads all requests using RequestReader and then writes new list
     * using the write method
     *
     * @param request to append
     * @see RequestReader
     * @throws IOException if something goes wrongs
     */
    public void append(Request request) throws IOException {
        List<Request> list = null;
        try {
            list = new RequestReader(path).readAll();
        } catch (Exception ex) {
            //if something goes wrong
            Logger.e(ex);
        } finally {
            if (list == null) {
                list = new LinkedList<>();
            }
            list.add(request);
            write(list);
        }
    }

    /**
     * Writes given request list to path
     *
     * @param requests to write
     * @throws IOException if something goes wrong while writing
     */
    public void write(List<Request> requests) throws IOException {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(path))) {
            oos.writeObject(requests);
            oos.flush();
        }
    }
}
