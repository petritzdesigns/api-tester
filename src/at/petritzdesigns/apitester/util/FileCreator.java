package at.petritzdesigns.apitester.util;

import java.io.File;
import java.io.IOException;

/**
 * File Creator utility class
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class FileCreator {

    /**
     * Creates path and file if they do not already exist
     *
     * @param pathStr the path
     * @throws IOException if path is invalid or path cannot be created
     */
    public static void createFileIfNotExist(String pathStr) throws IOException {
        if (pathStr.isEmpty()) {
            throw new IOException("Path is invalid.");
        }
        File path = new File(pathStr);

        if (!path.getParentFile().exists()) {
            path.getParentFile().mkdirs();
        }
        if (!path.exists()) {
            path.createNewFile();
        }
    }
}
