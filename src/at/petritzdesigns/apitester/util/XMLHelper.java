package at.petritzdesigns.apitester.util;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 * XML Helper utility class
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class XMLHelper {

    /**
     * Format given xml string
     *
     * @param xml the XML
     * @return formatted xml
     */
    public static String format(String xml) {
        if (xml.isEmpty()) {
            return "";
        }

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));

            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(OutputKeys.METHOD, "xml");
            xformer.setOutputProperty(OutputKeys.INDENT, "yes");
            xformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            xformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            Source source = new DOMSource(document);
            StringWriter stringWriter = new StringWriter();
            Result result = new StreamResult(stringWriter);
            xformer.transform(source, result);

            return stringWriter.toString();
        } catch (Exception ex) {
            return xml;
        }
    }
}
