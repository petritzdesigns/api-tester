package at.petritzdesigns.apitester.util;

import at.petritzdesigns.apitester.api.Request;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.List;

/**
 * Request Reader<br>
 * reads requests from given path
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @since 1.0.0
 */
public class RequestReader {

    /**
     * Path to read requests from
     */
    private final String path;

    /**
     * Default Constructor
     *
     * @param path the Path
     */
    public RequestReader(String path) {
        this.path = path;
    }

    /**
     * Read all requests from path
     *
     * @return list of requests
     * @throws Exception if something goes wrong while reading
     */
    public List<Request> readAll() throws Exception {
        try {
            List<Request> list = null;
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path));
            Object obj = ois.readObject();
            if (obj instanceof List) {
                list = (List<Request>) obj;
            }
            ois.close();

            return list;
        } catch (EOFException ex) {
            //Nothing to read from
            return null;
        }
    }
}
